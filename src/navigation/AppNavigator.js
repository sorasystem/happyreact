import React from 'react';
import { Image, TouchableOpacity, Platform } from 'react-native';
import { Constants } from 'expo';
import { StackNavigator } from 'react-navigation';
import Login from '../screens/Login';
import Home from '../screens/Home';
import PlaySong from '../screens/PlaySong';

const AppNavigator = StackNavigator(
    {
        Login: { screen: Login },
        Home: { screen: Home },
        PlaySong: { screen: PlaySong },
    },
    {
        navigationOptions: ({ navigation }) => ({
            headerStyle: {
                backgroundColor: '#eee',
                elevation: 0,
                borderBottomWidth: 0,
                shadowColor: 'transparent',
                paddingTop: Platform.select({ ios: 0, android: Constants.statusBarHeight }),
            },
            headerLeft: () => (
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Image source={require('../assets/ic_arrow_left.png')} style={{ width: 30, height: 30, margin: 15 }}/>
                </TouchableOpacity>
            ),
            headerTitleStyle: {
                fontSize: 16,
                alignSelf: 'center',
                marginRight: Platform.select({ ios: 10, android: 70 }),
            },
        })
    }
);

export default AppNavigator;
